package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class TarixMethodTests {

	@Test
	void testduplicate() {
		int [] [] matrix = new int [] [] {{ 1,2,3}, {4,5,6}} ;
		int [][] expected= new int [][] { { 1,2,3,1,2,3} , {4,5,6,4,5,6} };
		int [] [] copy = duplicate(matrix);
		 assertArrayEquals(expected, copy);
	}
}
