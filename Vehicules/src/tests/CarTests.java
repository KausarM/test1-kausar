package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Vehicles.Car;

class CarTests {

	@Test
	void testgetMethods() {
		//testing the getSpeed
		Car x = new Car (2);
		Car y = new Car (6);
		assertEquals(2, x.getSpeed());
		
		//testing the moveright void method
		x.moveRight();
		assertEquals(2,x.getLocation());
		x.moveRight();
		assertEquals(4,x.getLocation());
		
		//testing the moveleft void method
		y.moveLeft();
		assertEquals(-6, y.getLocation());
		y.moveLeft();
		assertEquals(-12, y.getLocation());
	}
	
	
	@Test
	void testaccelerateAndDecelerate() {
		//testing the accelerate void method
		Car x = new Car (3);
		Car y = new Car (4);
		assertEquals(3, x.getSpeed());
		x.accelerate();
		assertEquals(4,x.getSpeed());
		y.accelerate();
		y.accelerate();
		assertEquals(6,y.getSpeed());
		//testing the decelerate void method
		x.decelerate();
		x.decelerate();
		assertEquals(2,x.getSpeed());
		//testing if the speed will go to negatif or will remain at 0
		Car z = new Car (0);
		z.decelerate();
		z.decelerate();
		//if the test would have failed we would have gotten -2 as speed
		assertEquals(0,z.getSpeed());
		
}
}