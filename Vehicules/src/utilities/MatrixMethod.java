package utilities;

import java.util.Arrays;

 public class MatrixMethod {

	 public static int [][] duplicate (int [][] matrix) {
		 
		 int [][] duplicates = new int [matrix.length][];
					
		 for (int i = 0; i < matrix.length; i++) {
			 int [] temp = new  int [2*matrix[i].length];
			 
			 for ( int j=0; j< matrix[i].length; j++) {
				 temp[j]= matrix[i][j];
			 }
			 for ( int j=0; j< matrix[i].length; j++) {
				 temp[j+matrix[i].length]= matrix[i][j];
			 }
			duplicates[i]= temp;
				}	
		 return duplicates;
		 
	 }
	 /*  public static void main(String args[]) { 
		 int [] [] matrix = new int [] [] {{ 1,2,3}, {1,2,3}} ;
		 int [] [] copy = duplicate(matrix);
		 
		 for (int i = 0; i < copy.length; i++) {
		
			 for ( int j=0; j< copy[i].length; j++) {
				 System.out.print(copy[i][j] + " ");
			 }
			 System.out.println();
		 
	 }
	 } */
 }
	 
